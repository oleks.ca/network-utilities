#!/bin/bash

# Check if nmap is installed
if ! command -v nmap &> /dev/null; then
    echo "nmap is not installed. Please install nmap to continue."
    exit 1
fi

# Ask the user to enter an IP address
read -p "Enter the IP address for scanning: " ip_address

# Check if the IP address is provided
if [[ -z "$ip_address" ]]; then
    echo "No IP address provided. The script will now exit."
    exit 1
fi

# Display a message before starting the scan for open ports
echo "Scanning for open ports on the IP address $ip_address..."
port_scan_results=$(sudo nmap -sV $ip_address)
echo "Open Ports:"
echo "$port_scan_results" | grep -i "open"

# Separately, scan for OS information
echo "Attempting OS detection on $ip_address..."
os_scan_results=$(sudo nmap -O $ip_address)
echo "OS Information:"
# Directly show the OS scan results as filtering for guesses can be challenging without specific markers
sudo nmap -A $ip_address

# End of script
echo "Scan completed."
